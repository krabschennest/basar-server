# Krabschennest Basar Server

## Kleidermärkte 
Der [Förderverein der Kindertagesstätte und der Grundschule Langewiesen e.V.](https://www.krabschennest.de) veranstaltet regelmäßig [Kleidermärkte](https://www.krabschennest.de/?q=Kleidermarkt) (Frühjahr und Herbst) sowie eine Ski- und Spielzeugbörse während des Langewiesener Weihnachtsmarktes.

Hier haben private Verkäufer die Möglichkeit vor Beginn des jeweiligen Marktes eine Verkäufernummer über die Webseite des Vereins zu erhalten. Bis zum Beginn des Marktes können die Verkäufer die zuverkaufenden Artikel in einem Online Formular erfassen.

Die Daten der Verkäuferregistrierung und der erfassten Artikel werden zum Start des Marktes von einem Techniker des Vereins heruntergeladen und für die Kassensysteme bereitgestellt. Diese ergänzen während der Durchführung des Marktes welche Artikel verkauft wurden. Am Ende des Marktes müssen aus den so erfassten Daten die Abrechnungslisten für die Auszahlung und die Quittungen für den Verein und die Verkäufer erzeugt werden.

Die Basar Server App soll den Admin bzw. Techniker bei den notwendigen Arbeiten während der Durchführung des Marktes unterstützen.

## Features

Die Möglichkeiten, welche die Anwendung bietet sind dabei in den folgenden Abschnitten kurz erklärt.

### Import der aus der Marktverwaltung exportierten Datenbank

Zu Beginn des Marktes bzw. zur Inbetriebnahme der Kassen, muss die Datenbank für den aktuellen Markt aus der Marktverwaltung exportiert werden.
Der Export geschieht über ein entsprechendes PHP Skript, welches mittels mysqldump die wichtigsten Tabellen für den Markt exportiert und als Download
zurückliefert.

Dieses Dump muss anschließend in die lokale Datenbank auf dem Hauptrechner importiert werden. 

Bisher war es notwendig den Export herunterzuladen und auf den Hauptrechner zu kopieren (oder den Download direkt dort auszuführen) und anschließend mittels `mysql` Befehl auf der Konsole
in die lokale Datenbank zu importieren.

Mit der dieser App ist es nun möglich, den Export beispielsweise über das eigene Smartphone herunterzuladen, das Smartphone anschließend mit dem WLAN des Marktes zu verbinden und über diese Webanwendung
direkt in die Datenbank zu importieren. 

![Screenshot of the UI showing the import function (file upload input and button)](documentation/DBImport.png)

### Erstellung der Auswertungsliste

Am Ende des Marktes musste bisher eine Excel-(Open Document)-Tabelle erzeugt werden, die zum einen als Kontrollliste während der Auszahlung verwendet wurde. Außerdem diente sie aber auch als Basis für die Erzeugung der Quittungen. 

Für die Erzeugung lagen PHP Skripte auf dem Hauptrechner, die nach jedem Markt manuell angepasst werden mussten (IDs aus der Datenbank) und von denen weder eine Dokumentation noch eine Sicherung vorhanden war. 

Mit der Basar-Server App hat man nun die Möglichkeit die Auswertungsliste über die Webseite herunterzuladen, die von der App bereitgestellt wird.

![Screenshot of the UI showing excel report function ](documentation/DBExport.png)

### Erstellung der Quittungen

Früher musste aus dem Excel Report eine Libre Office BASE Datenbank erzeugt und diese mittels Serienbrief Funktion für die Erzeugung der Quittungen benutzt werden.

Mit der Basar-Server App kann man die Quittungen nun direkt über die Webseite erzeugen, die von der App bereitgestellt wird.

![Screenshot of the UI showing receipt function ](documentation/ReceiptExport.png)

### Verkäufer exportieren

Bereits am Ende des ersten Markttages sowie am Ende, möchten die Veräufer gern ihre Verkäufe online einsehen. Hierfür wurde bisher die Datenbank mittels `mysql` Befehl exportiert und anschließend mittels `curl` Befehl an die Marktverwaltungswebseite geschickt. 

Während der Upload aktuell noch nicht funktioniert, da der Hauptrechner keinen Verbindung zum Internet hat, kann zumindest die Erzeugung des Exports nun ebenfalls direkt über die Webseite der Basar-Server App ausgeführt werden. Allerdings wird statt dem php Serialisationsverfahren von früher jetzt json als Datenaustausch format benutzt.

![Screenshot of the UI showing the export Sales function ](documentation/SalesExport.png)

### Export der Datenbank

Diese Funktion stellt die Möglichkeit bereit die komplette Datenbank mittels `mysqldump` zu sichern und das Ergebnis herunter zu laden. 

![Screenshot of the UI showing the DB Export function ](documentation/DBExport.png)

## Voraussetzungen

Typischerweise besteht die Technik eines Marktes aus den Kassensystemen die per WLAN mit einem Hauptrechner verbunden sind, auf dem die Datenbank für den Markt betrieben wird. 

Seit der Ski- und Spielzeugbörse 2018 wird als Hauptrechner ein Raspberry Pi System genutzt, welches neben der Datenbank auch noch Dienste wie WLAN, DHCP und DNS bereitstellt. 

Der Einfachheithalber und da durch die Verwendung der Basar Server App keine größeren Lasten zu erwarten sind, wird die App direkt auf dem Raspberry Pi betrieben. 
