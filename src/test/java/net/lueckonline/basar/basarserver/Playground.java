package net.lueckonline.basar.basarserver;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

public class Playground {

  @Test
  public void test() throws IOException {
    
    ProcessBuilder processBuilder = 
          new ProcessBuilder()
          .command("/bin/bash", "-c", "mysqldump -u basar --password=basar --protocol=tcp -h localhost basar")
//          .inheritIO()
          ;
    Process process = processBuilder.start();
    
    IOUtils.copy(process.getInputStream(), new FileOutputStream("target/dump.sql"));
    
  }

}
