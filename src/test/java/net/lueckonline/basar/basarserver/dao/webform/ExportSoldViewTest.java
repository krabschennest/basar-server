package net.lueckonline.basar.basarserver.dao.webform;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.InflaterInputStream;

import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.lueckonline.basar.basarserver.dao.webform.ExportSoldView.SoldDTO;
import net.lueckonline.basar.basarserver.dao.webform.ExportSoldView.SoldItemDTO;

public class ExportSoldViewTest {

  private static final Logger logger = LogManager.getLogger();
  
  @Test
  public void test() throws Exception {
    ExportSoldView view = new ExportSoldView();

    Map<String,Object> map = new HashMap<>();
    List<BasarSold> sold = new ArrayList<>();
    map.put(ExportSoldView.EXPORTSOLD_MODEL_KEY, sold);
    
    WebformBasar basar = new WebformBasar(1, 0.1f);
    basar.setBasarId(1);
    
    sold.add(new BasarSold(basar, (short) 1, 1, "ABC", new Date()));
    sold.add(new BasarSold(basar, (short) 1, 2, "DEF", new Date()));
    
    MockHttpServletResponse response = new MockHttpServletResponse();
    
    view.renderMergedOutputModel(map, null, response);
    
    logger.debug("got the following response body {}", response.getContentAsString());
    
    try(InflaterInputStream is = new InflaterInputStream(new Base64InputStream(new ByteArrayInputStream(response.getContentAsByteArray())))) {
      
      String json = IOUtils.toString(is, "UTF-8");
      
      logger.debug("Found the following json in the response: {}", json);
      
      ObjectMapper om = new ObjectMapper();
      SoldDTO soldDTO = om.readValue(json, ExportSoldView.SoldDTO.class);
      
      assertNotNull(soldDTO);
      
      assertEquals(String.valueOf(basar.getBasarId()), soldDTO.getBasar());
      
      assertEquals(2, soldDTO.sold.size());
      
      SoldItemDTO si1 = soldDTO.sold.get(0);
      assertEquals("ABC", si1.getCode());
      assertEquals("1", si1.getHwID());
      assertEquals("1", si1.getRecno());
      
      SoldItemDTO si2 = soldDTO.sold.get(1);
      assertEquals("DEF", si2.getCode());
      assertEquals("1", si2.getHwID());
      assertEquals("2", si2.getRecno());
    }
  }
}
