package net.lueckonline.basar.basarserver.dao.webform;

import static net.lueckonline.basar.basarserver.dao.webform.WebformField.GIVENNAME;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.SALUTATION;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.SURNAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;

@DataJpaTest
public class WerbformBasarRepositoryTest {

//  @ClassRule
//  public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();
//  @Rule
//  public final SpringMethodRule springMethodRule = new SpringMethodRule();
  
  @Autowired
  WebformBasarRepository repository;

  @Test
  public void shouldFindNothingIfDbIsEmpty() {
    assertThat(repository.findAll()).isEmpty();
  }
  
  @Test
  public void insertAndFindAgain() {
    
    WebformBasar basar = new WebformBasar(1, 0.1f);
    
    WebformSubmittedData webformDataSalutation = new WebformSubmittedData(basar,2,SALUTATION,"Herr");
    WebformSubmittedData webformDataFirstName  = new WebformSubmittedData(basar,2,GIVENNAME ,"Leonardo");
    WebformSubmittedData webformDataLastName   = new WebformSubmittedData(basar,2,SURNAME   ,"da Vinci");
    
    List<WebformSubmittedData> input = 
        Stream.of(webformDataSalutation, webformDataFirstName, webformDataLastName)
              .collect(Collectors.toList());
    basar.getSubmittedData().addAll(input);
    
    List<BasarSold> bsInput = 
      Stream.iterate(
          BasarSold.builder().basar(basar).build(),
          seed -> {
            return seed.toBuilder()
                .recno(seed.getRecno() + 1)
                .code(UUID.randomUUID().toString())
                .build();
       })
      .limit(5)
      .collect(Collectors.toList());
      
    BasarSold bs1 = new BasarSold(basar,(short) 1, 1, "ABC", new Date());
    basar.getBasarSold().add(bs1);
    basar.getBasarSold().add(new BasarSold(basar,(short) 1, 2, "DEF", new Date()));
    
    repository.save(basar);
    
    Iterable<WebformBasar> basars = repository.findAll();
    assertThat(basars).size().isEqualTo(1);
    WebformBasar fetchedBasar = basars.iterator().next();
    assertThat(fetchedBasar.getSubmittedData()).isNotEmpty();
    fetchedBasar.getSubmittedData().forEach(w -> assertThat(input.contains(w)));
    
    fetchedBasar.getBasarSold().forEach(bs -> assertThat(bsInput.contains(bs)));
  }
  
  @ParameterizedTest
  @ValueSource(strings = {"0", "2", "100"})
  public void shouldThrowExceptionIfNotExactlyOneBasarInDB(int numberOfBasars) {
    
    repository.saveAll(
        Stream.iterate(new WebformBasar(1, 0.1f), b -> b.toBuilder().nodeId(b.getNodeId() + 1).build())
          .limit(numberOfBasars)
          .collect(Collectors.toList()));

    
    var thrown = assertThrows(InvalidDataAccessApiUsageException.class, () -> repository.findSingleBasar());
    
    assertThat(thrown.getMessage(), containsString("There must be exactly one basar in the database"));
  }
}
