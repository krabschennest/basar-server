package net.lueckonline.basar.basarserver.dao.webform;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class WebformFieldTest {

  @Test
  public void shouldMapToCorrectIntValues() {
    assertThat(WebformField.UNUSED0.ordinal()).isEqualTo(0);
    assertThat(WebformField.SALUTATION.ordinal()).isEqualTo(1);
    assertThat(WebformField.GIVENNAME.ordinal()).isEqualTo(2);
    assertThat(WebformField.SURNAME.ordinal()).isEqualTo(3);
    assertThat(WebformField.DATEOFBIRTH.ordinal()).isEqualTo(4);
    assertThat(WebformField.EMAIL.ordinal()).isEqualTo(5);
    assertThat(WebformField.STREET.ordinal()).isEqualTo(6);
    assertThat(WebformField.UNUSED7.ordinal()).isEqualTo(7);
    assertThat(WebformField.CITY.ordinal()).isEqualTo(8);
    assertThat(WebformField.PHONE.ordinal()).isEqualTo(9);
    assertThat(WebformField.PRIVACYNOTICEAGGREEMENT.ordinal()).isEqualTo(10);
    assertThat(WebformField.NEWSLETTERSUBSCRIPTION.ordinal()).isEqualTo(11);
    assertThat(WebformField.UNUSED12.ordinal()).isEqualTo(12);
    assertThat(WebformField.PASSWORD.ordinal()).isEqualTo(13);
    assertThat(WebformField.SELLERID.ordinal()).isEqualTo(14);
    assertThat(WebformField.UNUSED15.ordinal()).isEqualTo(15);
    assertThat(WebformField.POSTCODE.ordinal()).isEqualTo(16);
  }
}
