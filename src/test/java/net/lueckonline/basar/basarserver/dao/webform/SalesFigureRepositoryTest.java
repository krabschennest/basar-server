package net.lueckonline.basar.basarserver.dao.webform;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@DataJpaTest
@ExtendWith(SpringExtension.class)
public class SalesFigureRepositoryTest {

  @Autowired
  SalesFigureRepository repository;
  
  @Test
  public void shouldFetchAggregatedDataButWithoutArticleCount() {
    
    List<SalesFigure> soldFiguresForSellers = repository.getSoldFiguresForSellers().collect(Collectors.toList());
    
    assertThat(soldFiguresForSellers).isNotNull();
    assertThat(soldFiguresForSellers.size()).isEqualTo(3);
    assertThat(soldFiguresForSellers).contains(new SalesFigure(1, 131.52, 171.58, 5, 10));
    assertThat(soldFiguresForSellers).contains(new SalesFigure(2,  40.56, 179.83, 5, 11));
    assertThat(soldFiguresForSellers).contains(new SalesFigure(3,  46.71, 165.33, 5, 9));
  }

}
