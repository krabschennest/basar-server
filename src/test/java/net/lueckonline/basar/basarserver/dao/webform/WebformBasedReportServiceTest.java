package net.lueckonline.basar.basarserver.dao.webform;

import static net.lueckonline.basar.basarserver.dao.webform.WebformField.CITY;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.EMAIL;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.GIVENNAME;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.PHONE;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.POSTCODE;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.SALUTATION;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.SELLERID;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.STREET;
import static net.lueckonline.basar.basarserver.dao.webform.WebformField.SURNAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Stream;

import javax.money.Monetary;

import org.assertj.core.api.Condition;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import net.lueckonline.basar.basarserver.report.SellerReport;

@ExtendWith(SpringExtension.class)
public class WebformBasedReportServiceTest {

  @Mock
  WebformBasarRepository basarRepository;
  
  @Mock
  SalesFigureRepository salesFigureRepository;
  
  @InjectMocks
  private WebformBasedReportService service;

  @Test
  public void shouldReturnReportsSorted() {
    final WebformBasar testBasar = new WebformBasar(1, 0.0f);
    
    testBasar.getSubmittedData().add(new WebformSubmittedData(testBasar, 1, WebformField.SELLERID, "2"));
    testBasar.getSubmittedData().add(new WebformSubmittedData(testBasar, 2, WebformField.SELLERID, "10"));
    testBasar.getSubmittedData().add(new WebformSubmittedData(testBasar, 3, WebformField.SELLERID, "3"));
    testBasar.getSubmittedData().add(new WebformSubmittedData(testBasar, 4, WebformField.SELLERID, "0"));
    
    when(basarRepository.findSingleBasar()).thenAnswer(i -> testBasar);
    when(salesFigureRepository.getSoldFiguresForSellers()).then(i -> Stream.empty());
    
    List<SellerReport> report = service.report();
    
    assertThat(report).isSortedAccordingTo((e1,e2) -> Integer.compare(e1.getVk(), e2.getVk()));
    
  }
  
  @Test
  public void shouldAggregateFieldsOfWebFormSubmissionIntoSellerReportObjects() {
    
    /*
     MariaDB [basar]> select * from webform_submitted_data;
    +-----+-------+-----+----+-----------------------------------------+
    | nid | sid   | cid | no | data                                    |
    +-----+-------+-----+----+-----------------------------------------+
    | 330 | 10843 |   1 | 0  | Frau                                    |
    | 330 | 10843 |   2 | 0  | Lisa                                    |
    | 330 | 10843 |   3 | 0  | del Giocon                              |
    | 330 | 10843 |   4 | 0  | 1479-06-15                              |
    | 330 | 10843 |   5 | 0  | mona.lisa@louvre.fr                     |
    | 330 | 10843 |   6 | 0  | Rue de Rivoli                           |
    | 330 | 10843 |   8 | 0  | Paris                                   |
    | 330 | 10843 |   9 | 0  | 555-12345                               |
    | 330 | 10843 |  10 | 0  | Ja                                      |
    | 330 | 10843 |  11 | 0  | Ja                                      |
    | 330 | 10843 |  13 | 0  | passwd01                                |
    | 330 | 10843 |  14 | 0  | 1                                       |
    | 330 | 10843 |  15 | 0  | 75001                                   |
     */
    
    final WebformBasar basar = new WebformBasar(0,0.1f);
    
    for(int sellerId=0; sellerId < 10; sellerId++) {
      for(WebformField field : WebformField.values()) {
        final String data; 
        switch(field) {
          case DATEOFBIRTH: data = "1479-06-"+sellerId; break;
          case SELLERID: data = String.valueOf(sellerId); break;
          default: data = field.name() + sellerId; break;
        }
        basar.getSubmittedData().add(
            new WebformSubmittedData(basar, sellerId, field, data)
        );
      }
    }
    
    when(basarRepository.findSingleBasar()).thenAnswer(i -> basar);
    when(salesFigureRepository.getSoldFiguresForSellers()).then(i -> Stream.empty());
    
    Collection<SellerReport> report = service.report();
    
    assertThat(report).isNotNull();
    assertThat(report.size()).isEqualTo(10);
    
    for(int i=0; i< 10; i++) {
      final int sellerId = i;
      Condition<SellerReport> condition = new Condition<SellerReport>(s -> { 
        return (s.getSalutation().equals(SALUTATION.name() + sellerId)) && 
               (s.getGivenname().equals(GIVENNAME.name() + sellerId)) &&
               (s.getSurname().equals(SURNAME.name() + sellerId)) &&
               (s.getBirthday().equals(new GregorianCalendar(1479, 5, sellerId).getTime())) &&
               (s.getMail().equals(EMAIL.name() + sellerId)) &&
               (s.getStreet().equals(STREET.name() + sellerId)) &&
               (s.getCity().equals(CITY.name() + sellerId)) &&
               (s.getPostCode().equals(POSTCODE.name() + sellerId)) &&
               (s.getPhone().equals(PHONE.name() + sellerId)) &&
               (s.getVk() == sellerId);
      }, MessageFormat.format("Found no Seller Report that matches all criteria for sellerId {0}", sellerId)); 
      
      assertThat(report).areExactly(1, condition);
    }
  }
  @Test
  public void shouldAddSalesFiguresIfPresent() {

    //calculate a fraction based on the current value in the service ...
    float newFraction = service.getFoundationShareFraction() + 0.1f;
    // ... and set that new value in the service. We will test later that this new value is actually used
    service.setFoundationShareFraction(newFraction);
    
    WebformBasar testBasar = new WebformBasar(1, 0.0f);
    testBasar.getSubmittedData().add(new WebformSubmittedData(testBasar, 1, SELLERID, "1"));
    testBasar.getSubmittedData().add(new WebformSubmittedData(testBasar, 2, SELLERID, "2"));
    testBasar.getSubmittedData().add(new WebformSubmittedData(testBasar, 3, SELLERID, "3"));
    
    when(basarRepository.findSingleBasar()).thenAnswer(i -> testBasar);
    when(salesFigureRepository.getSoldFiguresForSellers()).then(i -> 
      Stream.of(new SalesFigure(1, 101.12, 151.2, 15, 30), new SalesFigure(2, 1.14, 5412.21, 1, 999))
    );
    
    List<SellerReport> report = service.report();
    
    assertThat(report.size()).isEqualTo(3);
    assertThat(report).isSortedAccordingTo((e1,e2) -> Integer.compare(e1.getVk(), e2.getVk()));
    assertThat(report).contains(SellerReport.builder()
                                        .vk(1)
                                        .soldSum(Money.of(101.12, "EUR"))
                                        .soldArticleCount(15)
                                        .foundationShareFraction(newFraction)
                                        .articleCount(30).build())
                      .contains(SellerReport.builder()
                                        .vk(2)
                                        .soldSum(Money.of(1.14, "EUR"))
                                        .soldArticleCount(1)
                                        .foundationShareFraction(newFraction)
                                        .articleCount(999).build())
                      .contains(SellerReport.builder()
                                        .vk(3)
                                        .soldSum(Money.of(0.0, "EUR"))
                                        .soldArticleCount(0)
                                        .foundationShareFraction(newFraction)
                                        .articleCount(0).build());
    assertThat(report).allSatisfy(r -> 
      assertThat(r.getSoldSum().multiply(newFraction).with(Monetary.getDefaultRounding())).isEqualTo(r.getFoundationShare())
    );
  }
  
  @Test
  public void shouldReturnTheResultsFromRepositoryWhenExportingSold() {
    final WebformBasar basar = new WebformBasar(0,0.1f);
    List<BasarSold> basarSold = basar.getBasarSold();
    basarSold.add(new BasarSold(basar, (short) 1, 1, "ABC", new Date()));
    basarSold.add(new BasarSold(basar, (short) 1, 2, "DEF", new Date()));
    
    when(basarRepository.findSingleBasar()).thenAnswer(i -> basar);
    
    service.exportSold().forEach(s -> assertThat(basarSold.contains(s)));
  }

}
