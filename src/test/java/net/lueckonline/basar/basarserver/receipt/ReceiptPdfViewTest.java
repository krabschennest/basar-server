package net.lueckonline.basar.basarserver.receipt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletResponse;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.github.javafaker.Faker;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;

import de.redsix.pdfcompare.PdfComparator;
import net.lueckonline.basar.basarserver.report.SellerReport;

@ExtendWith(SpringExtension.class)
public class ReceiptPdfViewTest {

  private Stream<SellerReport> testData;
  

  @BeforeEach
  public void setUp() {
    
    //use a random with a constan t seed. This ensures that we have the same 
    //data each time we run the test. 
    //this is important because we want to compare the resulting pdf to a reference pdf.
    Random random = new Random(1);
    //use the random with constant seed in the faker.
    Faker faker = new Faker(random);
    
    testData = Stream.iterate(
        //use an object that is build only with default values as seed 
        //this is to make sure that the implemenation does not expect more than the defaults
        SellerReport.builder().build(), 
        seed ->  {
          return seed.toBuilder()
                     .vk(seed.getVk() + 1)
                     .salutation(random.nextInt() % 2 == 0 ? "Frau" : "Herr")
                     .givenname(faker.name().firstName())
                     .surname(faker.name().lastName())
                     .street(faker.address().streetAddress())
                     .postCode(faker.address().zipCode())
                     .city(faker.address().city())
                     .birthday(faker.date().birthday(18, 65))
                     .mail(faker.internet().emailAddress())
                     .phone(faker.phoneNumber().cellPhone())
                     .articleCount(random.nextInt(500))
                     .soldArticleCount(random.nextInt(500))
                     .soldSum(Money.of(Math.abs(random.nextGaussian()) * 100, "EUR"))
//                     .foundationShareFraction(random.nextInt(100)/100.0f)
                     .foundationShareFraction(0.15f)
                     .build();
    });
  }
  
  @Test
  public void createPDF(TestInfo testInfo) throws Exception {
    
    /*
     * Arrange
     */
    HashMap<String, Object> model = new HashMap<>();
    model.put("sellerReport", testData.limit(10).collect(Collectors.toList()));
    model.put("basarName", "1. Testbasar ");
    

    HttpServletResponse response = mock(HttpServletResponse.class);
    
    /*
     * Act
     */
    ReceiptPdfView view = new ReceiptPdfView();
    
    Document document = view.newDocument();
    
    final byte[] byteBuffer;
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()){
      PdfWriter writer = PdfWriter.getInstance(document, baos);
      
      document.open();
      
      view.buildPdfDocument(model, document, writer, null, response);
      
      document.close();
      
      byteBuffer = baos.toByteArray();
    }
    
    document.close();
    
    /*
     * Assert
     */
    
    verify(response).setHeader("Content-Disposition", "attachment; filename=quittungen.pdf");
    
    final var testname = testInfo.getTestMethod().map(Method::getName).orElse("createPDF");
    
    try(InputStream actual = new ByteArrayInputStream(byteBuffer);
        InputStream expected = this.getClass().getResourceAsStream(testname+".pdf")) {
//      IOUtils.copy(actual, new FileOutputStream("result.pdf"));
      
      boolean isEqual  = new PdfComparator<>(expected, actual)
                            .compare().writeTo("target/"+testname+".diff");
      
      assertThat(isEqual).isEqualTo(true);
    }
  }
}
