package net.lueckonline.basar.basarserver.maintenance.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.sql.DataSource;

import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.lueckonline.basar.basarserver.maintenance.impl.KleidermarktFormatMaintenanceServiceTest.TestConfig;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { KleidermarktFormatMaintenanceService.class, TestConfig.class})
@TestPropertySource(locations = "classpath:/application.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class KleidermarktFormatMaintenanceServiceTest {

  @Configuration
  public static class TestConfig {
    @Bean
    public DataSource testDataSource() {
      var dataSource = new JdbcDataSource();
      dataSource.setUrl("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=false;MODE=MySQL");
      dataSource.setUser("sa");
      dataSource.setPassword("sa");
      return dataSource;
    }
    @Bean
    public ObjectMapper objectMapper() {
      return new ObjectMapper();
    }
  }
  
  @Autowired 
  KleidermarktFormatMaintenanceService service;
  
  @Autowired 
  DataSource dataSource;
  
  @Test
  @Order(value = 0)
  void shouldImportFile() throws Exception {
    
    final byte[] zippedBytes;
    try(var stream = this.getClass().getResourceAsStream("/kleidermarkt_export.json"); 
        var zippedStream = new ByteArrayOutputStream(); 
        var gzipStream = new GZIPOutputStream(zippedStream)) {
      stream.transferTo(gzipStream);
      gzipStream.finish();
      zippedBytes = zippedStream.toByteArray();
    }
    
    try(var stream = new ByteArrayInputStream(zippedBytes)) {
      final MultipartFile file = new MockMultipartFile("file", "export.json.gz", null, stream);
      service.importFile(file);
    }

    var jdbc = new JdbcTemplate(dataSource);
    var valid = jdbc.query("SELECT nid,basarid,open,opentill,name,shortname,prec,showsize,config FROM basar", rs -> {
      
      rs.next();
      assertThat(rs.getLong("nid"), is(1234l));
      assertThat(rs.getLong("basarid"), is(1234l));
      assertThat(rs.getInt("open"), is(1));
      assertThat(rs.getDate("opentill"), is(nullValue()));
      assertThat(rs.getString("name"), is("Frühjahrskleidermarkt 2022"));
      assertThat(rs.getString("shortname"), is("FKM 2022"));
      assertThat(rs.getFloat("prec"), is(0.1f));
      assertThat(rs.getInt("showsize"), is(1));
      assertThat(rs.getString("config"), is(nullValue()));
      
      assertThat("Only one entry in db expected", rs.next(), is(false));
      return true;
    });
    assertThat(valid, is(true));
    
    final List<Map<String,Object>> result = jdbc.query("SELECT basar,vk,article,price,description,size,locked,code from basar_items order by vk,article", (rs,i) -> {
      
      assertThat(rs.getLong("basar"), is(1234l));
      assertThat(rs.getInt("locked"), is(0));
      
      final var expectedVk = i < 2 ? 1234023539 :
                             i < 5 ? 1234078845 : -1;
      assertThat(rs.getInt("vk"), is(expectedVk));
      
      final var expectedArticle = i < 2 ? i + 1 :
                                  i < 5 ? i + 1-2 : -1;
      assertThat(rs.getInt("article"), is(expectedArticle));
      
      var map = new HashMap<String, Object>(Map.of(
        "price", rs.getDouble("price"),
        "description", rs.getString("description"),
        "code", rs.getString("code")
      ));
      if(rs.getString("size") != null) map.put("size", rs.getString("size"));
      
      return map;
    });
    
    var expectedValues = new ArrayList<Map<String,Object>>();
    try(var stream = this.getClass().getResourceAsStream("/kleidermarkt_export.json")) {
      JsonNode root = new ObjectMapper().readTree(stream);
      root.path("verkäufer").forEach(verkäuferNode -> {
        verkäuferNode.path("artikel").forEach(artikelNode -> {
          var nodeMap = new HashMap<String, Object>();
          nodeMap.put("description", artikelNode.get("beschreibung").asText());
          nodeMap.put("price", artikelNode.get("preis").asDouble());
          nodeMap.put("code", artikelNode.get("code").asText());
          expectedValues.add(nodeMap);
        });
      });
    }
    assertThat(result, hasSize(5));
  }
  
  @Test
  @Order(value = 1)
  void shouldExportFile() throws IOException {

    final var om = new ObjectMapper();
    
    final JsonNode reference;
    try(var refStream = this.getClass().getResourceAsStream("/kleidermarkt_import.json")) {
      reference = om.readTree(refStream);
    }
    var recno = 0;
    var jdbcTemplate = new JdbcTemplate(dataSource);
    jdbcTemplate.batchUpdate(
      "INSERT into basar_sold (basar,hwID,recno,code,timestamp) VALUES (1234,1,?,?,?)", List.of(
        new Object[] {recno++, "3472957330322529199", Instant.now()},
        new Object[] {recno++, "7311792174966817", Instant.now()},
        new Object[] {recno++, "5436138873728338573", Instant.now()},
        new Object[] {recno++, "8730163076848986157", Instant.now()}
    ));
   
    final JsonNode actual;
    try(var baos = new ByteArrayOutputStream()) {
      service.exportFile(baos);
      actual = om.readTree(new GZIPInputStream(new ByteArrayInputStream(baos.toByteArray())));
    }
    assertThat(reference, is(equalTo(actual)));
  }
  
  @Test
  void shouldReturnTrueOnCanExport() {
    assertThat(service.canExport(MediaType.parseMediaType("application/vnd.kleidermarkt+json;charset=UTF-8;encoding=gzip")), is(true));
  }
  
  @Test
  void shouldReturnTrueOnCanImport() {
    var mockFile = new MockMultipartFile("file", "foobar.json.gz", null, new byte[0]);
    assertThat(service.canImport(mockFile), is(true));  
  }
}
