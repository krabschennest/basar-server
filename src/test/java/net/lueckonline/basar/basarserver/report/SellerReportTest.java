package net.lueckonline.basar.basarserver.report;

import static org.assertj.core.api.Assertions.assertThat;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class SellerReportTest {

  @ParameterizedTest
  @CsvSource(value = {"15|15.00|2.25|12.75", "1.1234|1.12|0.17|0.95", "0.987654321|0.99|0.15|0.84"}, delimiterString = "|")
  public void test(double soldSum, double roundedSum, double foundationShare, double payout) {

    CurrencyUnit euro = Monetary.getCurrency("EUR");
    
    SellerReport sellerReport = SellerReport.builder()
                .soldSum(Money.of(soldSum, euro))
                .foundationShareFraction(0.15f)
                .build();
    
    assertThat(sellerReport.getSoldSum()).isEqualTo(Money.of(roundedSum, euro));
    assertThat(sellerReport.getFoundationShare()).isEqualTo(Money.of(foundationShare, euro));
    assertThat(sellerReport.getPayOut()).isEqualTo(Money.of(payout, euro));
  }
  
  /*
   * This is necessary because the field may not have been initialized when object is created from the database
   */
  @Test
  public void shouldNotThrowNPEWhenSellingNumbersFieldsAreAccessedWithoutFirstSettingThemExplicity() {
    SellerReport sellerReport = SellerReport.builder().build();
    
    assertThat(sellerReport.getSoldSum()).isEqualTo(Money.of(0.0, "EUR"));
    assertThat(sellerReport.getFoundationShare().isEqualTo(Money.of(0.0, "EUR")));
    assertThat(sellerReport.getPayOut().isEqualTo(Money.of(0.0, "EUR")));
    assertThat(sellerReport.getArticleCount()).isEqualTo(0);
  }

}
