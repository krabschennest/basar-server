package net.lueckonline.basar.basarserver.report;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.util.DateFormatConverter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.github.javafaker.Faker;

@ExtendWith(SpringExtension.class)
public class ReportExcelViewTest {
  
  private static final String TESTBASAR_NAME = "Frühjahrs Kleidermarkt 2019";

  private Stream<SellerReport> sellers;

  private Random random = new Random(new Date().getTime());
  
  @MockBean
  HttpServletResponse httpResponse;
  
  @BeforeEach
  public void setup() {
    
    Faker faker = new Faker();
    sellers = Stream.iterate( SellerReport.builder()
                                          .vk(0)
                                          .salutation("Herr")
                                          .surname("da Vinci")
                                          .givenname("Leonardo")
                                          .phone("555-1234")
                                          .street("Anchiano")
                                          .postCode("50059")
                                          .city("Vinci")
                                          .mail("leo@example.org")
                                          //da Vinci wasn't born 1952 but 500 hundred years earlier.
                                          //but as Excel doesn't handle dates before 1900 very well
                                          //and we probably won't need birthdates before 1900 we just
                                          //make the guy a bit younger ;-)
                                          .birthday(new GregorianCalendar(1952, 3, 14).getTime())
                                          .soldSum(Money.of(101.31, "EUR"))
                                          .foundationShareFraction(0.15f)
                                          .articleCount(100)
                                          .soldArticleCount(65)
                                          .build()
        , s -> {
                                          return s.toBuilder()
                                                   .vk(s.getVk()+1)
                                                   .salutation(s.getVk() % 2 == 0 ? "Herr" : "Frau")
                                                   .givenname(faker.name().firstName())
                                                   .surname(faker.name().lastName())
                                                   .phone(faker.phoneNumber().phoneNumber())
                                                   .street(faker.address().streetAddressNumber())
                                                   .postCode(faker.address().zipCode())
                                                   .city(faker.address().cityName())
                                                   .mail(faker.internet().emailAddress())
                                                   .birthday(faker.date().birthday())
                                                   .soldSum(Money.of(random.nextFloat() * 1000.0, "EUR"))
                                                   .articleCount(random.nextInt(200))
                                                   .soldArticleCount(random.nextInt(200))
                                                   .build();
        });
  }
  
  @Test
  public void shouldCreateWorkbookWithTenLines() throws Exception {
    
    /*
     * Arrange
     */
    ReportExcelView view = new ReportExcelView();
    
    final Date exportDateTime = new Date();
    final int numberOfHeaderRows = /*Basarname*/ 1 + /*export datetime*/ 1 + /*header*/ 1;

    List<SellerReport> sellerList = sellers.limit(10).collect(Collectors.toList());
    
    Map<String, Object> model = new HashMap<>();
    model.put(ReportExcelView.SELLERREPORT_MODEL_KEY, sellerList );
    model.put(ReportExcelView.SELLERREPORT_BASAR_NAME_KEY, TESTBASAR_NAME);
    model.put(ReportExcelView.SELLERREPORT_EXPORT_DATETIME_KEY, exportDateTime);
    XSSFWorkbook workbook = new XSSFWorkbook();
    
    /*
     * Act
     */
    view.buildExcelDocument(model, workbook, null, httpResponse);
    
    /*
     * Assert
     */
    
    verify(httpResponse).setHeader("Content-Disposition", "attachment; filename=abrechnung.xlsx");

    assertThat(workbook.getNumberOfSheets()).isEqualTo(1);

    XSSFSheet sheet = workbook.getSheetAt(0);
    assertThat(sheet.getSheetName()).isEqualTo("Basarabrechnung");
    
    CellStyle currencyCellStyle = workbook.createCellStyle();
    DataFormat currencyDateFormat = workbook.createDataFormat();
    currencyCellStyle.setDataFormat(currencyDateFormat.getFormat("0.#0 [$€-407]"));

    assertThat(sheet.getColumnStyle(0)).isEqualTo(currencyCellStyle);
    assertThat(sheet.getColumnStyle(1)).isEqualTo(currencyCellStyle);
    assertThat(sheet.getColumnStyle(2)).isEqualTo(currencyCellStyle);
    
    CellStyle dateCellStyle = workbook.createCellStyle();
    DataFormat dateDataFormat = workbook.createDataFormat();
    dateCellStyle.setDataFormat(dateDataFormat.getFormat(DateFormatConverter.convert(Locale.GERMANY, "dd.MM.yyyy")));
    
    assertThat(sheet.getColumnStyle(10)).isEqualTo(dateCellStyle);
    
    assertThat(sheet.getRow(1).getCell(0).getCellStyle()).isEqualTo(dateCellStyle);
    
    int lastRowNum = sheet.getLastRowNum();
    
    assertThat(lastRowNum + 1).isEqualTo(numberOfHeaderRows + sellerList.size());
    
    assertThat(sheet.getRow(0).getCell(0).getStringCellValue()).isEqualTo(TESTBASAR_NAME);
    assertThat(sheet.getRow(1).getCell(0).getDateCellValue()).isEqualTo(exportDateTime);
    
    final XSSFRow headerRow = sheet.getRow(2);
    assertThat(headerRow.getCell( 0).getStringCellValue()).isEqualTo("verkaufsSumme");
    assertThat(headerRow.getCell( 1).getStringCellValue()).isEqualTo("anteilVerein");
    assertThat(headerRow.getCell( 2).getStringCellValue()).isEqualTo("auszahlungsBetrag");
    assertThat(headerRow.getCell( 3).getStringCellValue()).isEqualTo("artikelZahl"); 
    assertThat(headerRow.getCell( 4).getStringCellValue()).isEqualTo("plz");
    assertThat(headerRow.getCell( 5).getStringCellValue()).isEqualTo("vk");
    assertThat(headerRow.getCell( 6).getStringCellValue()).isEqualTo("phone");
    assertThat(headerRow.getCell( 7).getStringCellValue()).isEqualTo("city");
    assertThat(headerRow.getCell( 8).getStringCellValue()).isEqualTo("street");
    assertThat(headerRow.getCell( 9).getStringCellValue()).isEqualTo("mail");
    assertThat(headerRow.getCell(10).getStringCellValue()).isEqualTo("geburtsdatum");
    assertThat(headerRow.getCell(11).getStringCellValue()).isEqualTo("surname");
    assertThat(headerRow.getCell(12).getStringCellValue()).isEqualTo("givenname");
    assertThat(headerRow.getCell(13).getStringCellValue()).isEqualTo("anrede");
    
    for(int sellerIdx=0; sellerIdx<sellerList.size(); sellerIdx++) {
      XSSFRow sellerRow = sheet.getRow(numberOfHeaderRows + sellerIdx);
      SellerReport sellerReport = sellerList.get(sellerIdx);
      
      double sumCellValue    = sellerRow.getCell( 0).getNumericCellValue();
      double shareCellValue  = sellerRow.getCell( 1).getNumericCellValue();
      double payOutCellValue = sellerRow.getCell( 2).getNumericCellValue();
      
      assertThat(sumCellValue).isEqualTo(sellerReport.getSoldSum().getNumber().doubleValue());
      assertThat(shareCellValue).isEqualTo(sellerReport.getFoundationShare().getNumber().doubleValue());
      assertThat(payOutCellValue).isEqualTo(sellerReport.getPayOut().getNumber().doubleValue());
      
      CurrencyUnit currencyEuro = Monetary.getCurrency("EUR");
      assertThat(Money.of(shareCellValue, currencyEuro).add(Money.of(payOutCellValue, currencyEuro)))
      .isEqualTo(Money.of(sumCellValue, currencyEuro));
      
      assertThat(sellerRow.getCell( 3).getNumericCellValue()).isEqualTo(sellerReport.getArticleCount());
      assertThat(sellerRow.getCell( 4).getStringCellValue()).isEqualTo(sellerReport.getPostCode());
      assertThat(sellerRow.getCell( 5).getNumericCellValue()).isEqualTo(sellerReport.getVk());
      assertThat(sellerRow.getCell( 6).getStringCellValue()).isEqualTo(sellerReport.getPhone());
      assertThat(sellerRow.getCell( 7).getStringCellValue()).isEqualTo(sellerReport.getCity());
      assertThat(sellerRow.getCell( 8).getStringCellValue()).isEqualTo(sellerReport.getStreet());
      assertThat(sellerRow.getCell( 9).getStringCellValue()).isEqualTo(sellerReport.getMail());
      assertThat(sellerRow.getCell(10).getDateCellValue()).isEqualTo(sellerReport.getBirthday());
      assertThat(sellerRow.getCell(11).getStringCellValue()).isEqualTo(sellerReport.getSurname());
      assertThat(sellerRow.getCell(12).getStringCellValue()).isEqualTo(sellerReport.getGivenname());
      assertThat(sellerRow.getCell(13).getStringCellValue()).isEqualTo(sellerReport.getSalutation());
    }
    
    workbook.write(new FileOutputStream("result.xlsx"));
  }

}
