package net.lueckonline.basar.basarserver;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import net.lueckonline.basar.basarserver.maintenance.MaintenanceService;
import net.lueckonline.basar.basarserver.receipt.ReceiptPdfView;
import net.lueckonline.basar.basarserver.report.ReportExcelView;
import net.lueckonline.basar.basarserver.report.ReportService;
import net.lueckonline.basar.basarserver.report.SellerReport;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = MainController.class)
public class MainControllerTest {

  @MockBean
  ReportService reportService;
  
  @MockBean
  MaintenanceService importService;
  
  @Autowired
  MockMvc mockmvc;
  
  @Test
  @WithMockUser
  public void importKleidermarktJson() throws Exception {

    var file = new MockMultipartFile("file", "test.json.gz", null, new byte[0]);
    
    when(importService.canImport(file)).thenReturn(true);
    
    mockmvc.perform(multipart(URI.create("/importfile")).file(file).with(csrf()))
      .andExpect(status().is3xxRedirection())
      .andExpect(header().string(HttpHeaders.LOCATION, is("/")));

    verify(importService).importFile(file);
  }
  
  @Test
  @WithMockUser
  public void exportKleiderMarktJson() throws Exception {
    
    var expectedMediaType = MediaType.parseMediaType("application/vnd.kleidermarkt+json;charset=UTF-8;encoding=gzip");
    when(importService.canExport(expectedMediaType)).thenReturn(true);
    
    mockmvc.perform(get("/exportdb").param("format", "kleidermarkt"))
    .andExpect(status().isOk())
    .andExpect(content().contentType(expectedMediaType))
    .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, is("attachment; filename=basar.json.gz")));
    
    verify(importService).exportFile(notNull());
  }
  
  @Test
  public void testReport() throws Exception {
    
    Date before = new Date();
    
    when(reportService.getBasarName()).then(i -> "TestBasar");
    
    List<SellerReport> reportList = Collections.singletonList(SellerReport.builder().build());
    when(reportService.report()).then(i -> reportList);
    
    mockmvc.perform(get("/report").with(user("Me")))
           .andExpect(status().isOk())
           .andExpect(content().contentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
           .andExpect(header().string("Content-Disposition", "attachment; filename=abrechnung.xlsx"))
           .andExpect(model().attribute(ReportExcelView.SELLERREPORT_MODEL_KEY, is(sameInstance(reportList))))
           .andExpect(model().attribute(ReportExcelView.SELLERREPORT_BASAR_NAME_KEY, is(equalTo("TestBasar"))))
           .andExpect(model().attribute(ReportExcelView.SELLERREPORT_EXPORT_DATETIME_KEY, is(greaterThanOrEqualTo(before))))
           ;
  }
  
  @Test
  public void testReceipt() throws Exception {
    
    when(reportService.getBasarName()).then(i -> "TestBasar");
    
    List<SellerReport> reportList = Collections.singletonList(SellerReport.builder().build());
    when(reportService.report()).then(i -> reportList);
    
    mockmvc.perform(get("/receipt").with(user("Me")))
           .andExpect(status().isOk())
           .andExpect(content().contentType("application/pdf"))
           .andExpect(header().string("Content-Disposition", "attachment; filename=quittungen.pdf"))
           .andExpect(model().attribute(ReceiptPdfView.SELLERREPORT_MODEL_KEY, is(sameInstance(reportList))))
           .andExpect(model().attribute(ReceiptPdfView.SELLERREPORT_BASAR_NAME_KEY, is(equalTo("TestBasar"))))
           ;
    
  }
}
