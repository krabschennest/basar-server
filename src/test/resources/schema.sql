--
-- Table structure for table `basar`
--

DROP TABLE IF EXISTS `basar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basar` (
  `nid` smallint(6) NOT NULL,
  `basarID` int(11) NOT NULL AUTO_INCREMENT,
  `open` tinyint(1) DEFAULT 0,
  `openTill` datetime DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `shortname` varchar(20) DEFAULT NULL,
  `prec` float unsigned NOT NULL,
  `showSize` int(11) DEFAULT NULL,
  `config` text DEFAULT NULL,
  PRIMARY KEY (`basarID`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `basar_cashCount`
--

DROP TABLE IF EXISTS `basar_cashCount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basar_cashCount` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `basarID` int(11) NOT NULL,
  `hwID` int(11) NOT NULL,
  `countTime` datetime NOT NULL,
  `person` varchar(20) NOT NULL,
  `reason` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `basar_items`
--

DROP TABLE IF EXISTS `basar_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basar_items` (
  `basar` smallint(6) DEFAULT NULL,
  `vk` int(11) DEFAULT NULL,
  `article` smallint(6) DEFAULT NULL,
  `price` float(2) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `size` varchar(10) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `code` varchar(25) NOT NULL,
  PRIMARY KEY (`code`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `basar_sold`
--

DROP TABLE IF EXISTS `basar_sold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basar_sold` (
  `basar` int(11) NOT NULL,
  `hwID` tinyint(4) NOT NULL,
  `recno` int(11) NOT NULL,
  `code` varchar(25) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`code`),
  UNIQUE KEY `code` (`code`)
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `webform_submitted_data`
--

DROP TABLE IF EXISTS `webform_submitted_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webform_submitted_data` (
  `nid` int(10) unsigned NOT NULL DEFAULT 0 ,
  `sid` int(10) unsigned NOT NULL DEFAULT 0 ,
  `cid` smallint(5) unsigned NOT NULL DEFAULT 0 ,
  `no` varchar(128) NOT NULL DEFAULT '0' ,
  `data` mediumtext NOT NULL ,
  PRIMARY KEY (`nid`,`sid`,`cid`,`no`)
);
/*!40101 SET character_set_client = @saved_cs_client */;