CREATE OR REPLACE TABLE basar (
  `nid` smallint(6) NOT NULL,
  `basarID` int(11) NOT NULL AUTO_INCREMENT,
  `open` tinyint(1) DEFAULT 0,
  `openTill` datetime DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `shortname` varchar(20) DEFAULT NULL,
  `prec` float unsigned NOT NULL,
  `showSize` int(11) DEFAULT NULL,
  `config` text DEFAULT NULL,
  PRIMARY KEY (`basarID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE OR REPLACE TABLE `basar_items` (
  `basar` smallint(6) DEFAULT NULL,
  `vk` int(11) DEFAULT NULL,
  `article` smallint(6) DEFAULT NULL,
  `price` float(2) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `size` varchar(10) DEFAULT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `code` varchar(25) NOT NULL,
  PRIMARY KEY (`code`),
  UNIQUE KEY `code` (`code`),
  KEY `vk` (`vk`),
  KEY `locked` (`locked`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8; 

CREATE OR REPLACE TABLE `basar_sold` (
  `basar` int(11) NOT NULL,
  `hwID` tinyint(4) NOT NULL,
  `recno` int(11) NOT NULL,
  `code` varchar(25) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`code`),
  KEY `basar` (`basar`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE OR REPLACE TABLE `basar_cashCount` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `basarID` int(11) NOT NULL,
  `hwID` int(11) NOT NULL,
  `countTime` datetime NOT NULL,
  `person` varchar(20) NOT NULL,
  `reason` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `comment` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;