package net.lueckonline.basar.basarserver.receipt;

import java.awt.Color;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.javamoney.moneta.format.CurrencyStyle;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import net.lueckonline.basar.basarserver.report.SellerReport;

public class ReceiptPdfView extends AbstractPdfView {

  public static final String SELLERREPORT_MODEL_KEY = "sellerReport";
  public static final String SELLERREPORT_BASAR_NAME_KEY = "basarName";
  
  private final Font fontHeadline;
  private final Font fontAddress;
  private final Font fontReceipt;
  private final Font fontSellerNr;
  private final Font fontBasar;
  private final Font fontText;
  private final Font payoutFont;
  private final Font fontSignatureLbl;
  private final MonetaryAmountFormat customFormat;

  public ReceiptPdfView() {
    super();
    fontHeadline    = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10.0f, new Color(0x00,0x00,0xFF));
    fontAddress     = FontFactory.getFont(FontFactory.HELVETICA, 8.0f, new Color(0x80,0x80,0x80));
    fontReceipt     = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 16.0f, new Color(0xFF,0x00,0x00));
    fontSellerNr    = FontFactory.getFont(FontFactory.HELVETICA, 16.0f, new Color(0x00,0x00,0x00));
    fontBasar       = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 16.0f, new Color(0x00,0x00,0x00));
    fontText        = FontFactory.getFont(FontFactory.HELVETICA, 10.0f, new Color(0x00,0x00,0x00));
    payoutFont      = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 10.0f, new Color(0x00,0x00,0x00));
    fontSignatureLbl= FontFactory.getFont(FontFactory.HELVETICA, 6.0f, new Color(0x00,0x00,0x00));
    
    customFormat = MonetaryFormats.getAmountFormat(AmountFormatQueryBuilder.
        of(Locale.GERMANY).set(CurrencyStyle.SYMBOL).set("pattern", "0.00 ¤").build());
  }

  @Override
  protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    
    @SuppressWarnings("unchecked") //TODO: make the model an actual calls itself
    List<SellerReport> sellerList = (List<SellerReport>) model.get("sellerReport");
    
    String basarName = (String) model.get("basarName");
    
    for(SellerReport report : sellerList)
      createReceiptPage(document, writer, basarName, report);
    
    response.setHeader("Content-Disposition", "attachment; filename=quittungen.pdf");
    
  }

  private void createReceiptPage(Document document, PdfWriter writer, String basarName, SellerReport report) {
    
    document.newPage();
    
    PdfContentByte cb = writer.getDirectContent();
    
    createReceipt(document, basarName, cb, report, true);
    
    cb.setLineDash(3,3,3);
    cb.setLineWidth(0f);
    cb.moveTo(0, document.getPageSize().getHeight()/2);
    cb.lineTo(document.getPageSize().getWidth(), document.getPageSize().getHeight()/2);
    cb.stroke();
    
    createReceipt(document, basarName, cb, report, false);
  }

  private void createReceipt(Document document, String basarName, PdfContentByte cb, SellerReport report, boolean seller) {
    
    cb.setLineDash(1,0,0);
    
    document.add(new Paragraph("Förderverein der Kindertagesstätte und der Grundschule Langewiesen e.V.",fontHeadline));
    document.add(new Paragraph("Friedrich Eck Str. 14b", fontAddress));
    document.add(new Paragraph("98704 Langewiesen", fontAddress));
    document.add(new Paragraph(""));
    
    PdfPTable table = new PdfPTable(4);
    table.setWidthPercentage(100.0f);
    table.setWidths(new float[]{15.0f, 35.0f, 20.0f, 30.0f});
    table.setSpacingBefore(15.0f);
    table.setSpacingAfter(15.0f);
    /* ----------------------------------------------------------------------
     |  Quittung                       |             für Verkäufer Nr.: <VK>|
     */
    
    PdfPCell receiptCell = new PdfPCell(new Paragraph("Quittung", fontReceipt));
    receiptCell.setColspan(2);
    receiptCell.setBorder(Rectangle.NO_BORDER);
    table.addCell(receiptCell);

    PdfPCell cellSellerNr = new PdfPCell(new Paragraph("für Verkäufer Nr.:"+report.getVk(), fontSellerNr));
    cellSellerNr.setColspan(2);
    cellSellerNr.setHorizontalAlignment(Element.ALIGN_RIGHT);
    cellSellerNr.setFixedHeight(15);
    cellSellerNr.setBorder(Rectangle.NO_BORDER);
    table.addCell(cellSellerNr);
    /* ----------------------------------------------------------------------
    |                             X. Test Basar                              |
    */
    
    PdfPCell basarCell = new PdfPCell(new Paragraph(basarName, fontBasar));
    basarCell.setColspan(4);
    basarCell.setHorizontalAlignment(Element.ALIGN_CENTER);
    basarCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
    basarCell.setFixedHeight(25);
    basarCell.setBorder(Rectangle.NO_BORDER);
    table.addCell(basarCell);
    
    /* ----------------------------------------------------------------------
    |  Verkäufer   | <givenname> <surname>                                  |
    */

    PdfPCell sellerCell = new PdfPCell(new Paragraph("Verkäufer", fontText));
    sellerCell.setBorder(Rectangle.NO_BORDER);
    table.addCell(sellerCell);
    
    PdfPCell nameCell = new PdfPCell(new Paragraph(report.getGivenname()+ " "+report.getSurname(), fontText));
    nameCell.setColspan(3);
    nameCell.setBorder(Rectangle.NO_BORDER);
    nameCell.setFixedHeight(15f);
    table.addCell(nameCell);
    
    /* ----------------------------------------------------------------------
    |  &nbsp      | <street>                                                |
    */
    PdfPCell emptyCell = new PdfPCell(new Paragraph("", fontText));
    emptyCell.setBorder(Rectangle.NO_BORDER);
    table.addCell(emptyCell);
    
    PdfPCell streetCell = new PdfPCell(new Paragraph(report.getStreet(), fontText));
    streetCell.setBorder(Rectangle.NO_BORDER);
    streetCell.setColspan(3);
    streetCell.setFixedHeight(15f);
    table.addCell(streetCell);
    
    /* ----------------------------------------------------------------------
    |  &nbsp      | <plz> <city>                                            |
    */
    table.addCell(emptyCell);
    
    PdfPCell postCodeCityCell = new PdfPCell(new Paragraph(report.getPostCode() + " " + report.getCity(), fontText));
    postCodeCityCell.setBorder(Rectangle.NO_BORDER);
    postCodeCityCell.setColspan(3);
    postCodeCityCell.setFixedHeight(15f);
    table.addCell(postCodeCityCell);

    /* ----------------------------------------------------------------------
    |  &nbsp                                                                |
    */
    
    PdfPCell emptyRow = new PdfPCell(new Paragraph(""));
    emptyRow.setColspan(4);
    emptyRow.setBorder(Rectangle.NO_BORDER);
    emptyRow.setFixedHeight(15f);
    table.addCell(emptyRow);
    
    /* ----------------------------------------------------------------------
    |  &nbsp      |Verkaufssumme:|___________<soldSum><CUR>|                |
    */
    table.addCell(emptyCell);
    PdfPCell soldSumLabel = new PdfPCell(new Paragraph("Verkaufssumme:", fontText));
    soldSumLabel.setBorder(Rectangle.NO_BORDER);
    table.addCell(soldSumLabel);
    
    PdfPCell soldSumCell = new PdfPCell(new Paragraph(customFormat.format(report.getSoldSum()), fontText));
    
    soldSumCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    soldSumCell.setBorder(Rectangle.BOTTOM);
    table.addCell(soldSumCell);
    table.addCell(emptyCell);
    
    /* ----------------------------------------------------------------------
    |  &nbsp      |15% des Vereins:|___________<foundationShare>|            |
    */
    table.addCell(emptyCell);
    String sharePercent = String.format("%.0f %% des Vereins:", report.getFoundationShareFraction() * 100);
    
    PdfPCell shareLabel = new PdfPCell(new Paragraph(sharePercent, fontText));
    shareLabel.setBorder(Rectangle.NO_BORDER);
    table.addCell(shareLabel);
    PdfPCell shareCell = new PdfPCell(new Paragraph(customFormat.format(report.getFoundationShare()), fontText));
    shareCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    shareCell.setBorder(Rectangle.BOTTOM);
    table.addCell(shareCell);
    table.addCell(emptyCell);
    
    /* ----------------------------------------------------------------------
    |  &nbsp      |Auszahlungsbetrag:|___________<payout>|                  |
    */
    table.addCell(emptyCell);
    PdfPCell payoutLabel = new PdfPCell(new Paragraph("Auszahlungsbetrag:", fontText));
    payoutLabel.setBorder(Rectangle.NO_BORDER);
    table.addCell(payoutLabel);
    PdfPCell payoutCell = new PdfPCell(new Paragraph(customFormat.format(report.getPayOut()), payoutFont));
    payoutCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    payoutCell.setBorder(Rectangle.BOTTOM);
    table.addCell(payoutCell);
    table.addCell(emptyCell);

    if(seller) {
    
      /* ----------------------------------------------------------------------
      |                                |                                       |
      |                                |                                       |
      |       Betrag dankend erhalten: |_______________________________________|
      */
      
      PdfPCell amountReceivedLeft = new PdfPCell(new Paragraph("Betrag dankend erhalten:", fontText));
      amountReceivedLeft.setHorizontalAlignment(Element.ALIGN_RIGHT);
      amountReceivedLeft.setVerticalAlignment(Element.ALIGN_BOTTOM);
      amountReceivedLeft.setColspan(2);
      amountReceivedLeft.setFixedHeight(60.0f);
      amountReceivedLeft.setBorder(Rectangle.NO_BORDER);
      table.addCell(amountReceivedLeft);
      
      PdfPCell signatureFieldSeller = new PdfPCell(emptyCell);
      signatureFieldSeller.setColspan(2);
      signatureFieldSeller.setBorder(Rectangle.NO_BORDER);
      table.addCell(signatureFieldSeller);
      
      /*
      |                                |      Unterschrift Verkäufer           |
       */
    
      table.addCell(emptyCell); 
      table.addCell(emptyCell);
      PdfPCell labelSellerSignCell = new PdfPCell(new Paragraph("Unterschrit Verkäufer", fontSignatureLbl));
      labelSellerSignCell.setColspan(2);
      labelSellerSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
      labelSellerSignCell.setBorder(Rectangle.TOP);
      table.addCell(labelSellerSignCell);
    } else {
    
      /* ----------------------------------------------------------------------
      |                                   |                                    |
      |                                   |                                    |
      |Datum, Unterschrift, Stempel Verein|____________________________________|
      */
      
      PdfPCell amountPayedLeft = new PdfPCell(new Paragraph("Datum, Unterschrift, Stempel Verein:", fontText));
      amountPayedLeft.setHorizontalAlignment(Element.ALIGN_RIGHT);
      amountPayedLeft.setVerticalAlignment(Element.ALIGN_BOTTOM);
      amountPayedLeft.setColspan(2);
      amountPayedLeft.setFixedHeight(60.0f);
      amountPayedLeft.setBorder(Rectangle.NO_BORDER);
      table.addCell(amountPayedLeft);
      
      PdfPCell signatureFieldFoundation = new PdfPCell(emptyCell);
      signatureFieldFoundation.setColspan(2);
      signatureFieldFoundation.setBorder(Rectangle.NO_BORDER);
      table.addCell(signatureFieldFoundation);
  
      /*
      |                                |      Förderverein                     |
       */
      table.addCell(emptyCell); 
      table.addCell(emptyCell);
      PdfPCell labelFoundationSignCell = new PdfPCell(new Paragraph("Förderverein", fontSignatureLbl));
      labelFoundationSignCell.setColspan(2);
      labelFoundationSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
      labelFoundationSignCell.setBorder(Rectangle.TOP);
      table.addCell(labelFoundationSignCell);
      /* ----------------------------------------------------------------------*/
    }
    
    document.add(table);
  }

  @Override
  protected Document newDocument() {
    Document document = new Document(PageSize.A5);
    document.setMargins(10, 10, 10, 10);
    return document;
  }
  

}
