package net.lueckonline.basar.basarserver;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.lueckonline.basar.basarserver.dao.webform.BasarSold;
import net.lueckonline.basar.basarserver.dao.webform.ExportSoldView;
import net.lueckonline.basar.basarserver.maintenance.MaintenanceService;
import net.lueckonline.basar.basarserver.receipt.ReceiptPdfView;
import net.lueckonline.basar.basarserver.report.ReportExcelView;
import net.lueckonline.basar.basarserver.report.ReportService;

@Controller
public class MainController {

  private final Logger logger = LogManager.getLogger();
  
  private final List<MaintenanceService> maintenanceServices;
  
  private final ReportService reportService;
  
  @Autowired
  private MessageSource messageSource;
  
  @Autowired
  public MainController(List<MaintenanceService> maintenanceServices, ReportService reportService) {
      this.maintenanceServices = maintenanceServices;
      this.reportService = reportService;
  }
  
  @GetMapping("/")
  public String index() {
    //TODO make sure that the import functionality ist not the default option when a db has already been import/there is already a db in the dbms
    return "index";
  }
  
  @GetMapping("/login")
  public String login() {
    return "login";
  }
  
  @GetMapping("/logout")
  public String logout() {
    return "redirect:/";
  }
  
  @PostMapping("/importfile")
  public String upload(@NotNull @RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes, Locale locale) {
    
    try {
      var service = maintenanceServices.stream().filter(ms -> ms.canImport(file)).findFirst().orElseThrow(() -> new IllegalArgumentException("No service found to import file "));
      service.importFile(file);
      redirectAttributes.addFlashAttribute("message", messageSource.getMessage("upload.success", new String[]{file.getOriginalFilename()}, locale)); 
    } catch (Exception e) {
      logger.error("Error while uploading file {}", e.getMessage(), e);
      redirectAttributes.addFlashAttribute("message", messageSource.getMessage("upload.error", new String[]{file.getOriginalFilename()}, locale));
    }

    return "redirect:/";
  }
  
  @GetMapping(path = "/exportdb")
  public void exportDb(@RequestParam(name="format", defaultValue = "mysql") String format, HttpServletResponse response) throws IOException {
    
    final var mediaType = "kleidermarkt".equals(format) ? "application/vnd.kleidermarkt+json;charset=UTF-8;encoding=gzip" : "application/sql";
    final var fileExt   = "kleidermarkt".equals(format) ? "json.gz" :  "sql";
    
    final var maintenanceService = this.maintenanceServices.stream()
       .filter(ms -> ms.canExport(MediaType.parseMediaType(mediaType))).findFirst()
       .orElseThrow(() -> new RuntimeException(String.format("no maintenance service for %s found",mediaType))); 

    response.setContentType(mediaType.toString());
    response.setHeader("Content-Disposition", String.format("attachment; filename=basar.%s",fileExt));
    
    maintenanceService.exportFile(response.getOutputStream());
  }
  
  @GetMapping("/exportsold")
  public ModelAndView exportsold() throws IOException {
    List<BasarSold> sold = this.reportService.exportSold();
    Map<String, Object> model = new HashMap<>();
    model.put(ExportSoldView.EXPORTSOLD_MODEL_KEY,sold);
    return new ModelAndView(new ExportSoldView(), model);
  }
  
  @GetMapping("/report")
  public ModelAndView report() {
    Map<String, Object> model = new HashMap<>();
    model.put(ReportExcelView.SELLERREPORT_MODEL_KEY, reportService.report());
    model.put(ReportExcelView.SELLERREPORT_EXPORT_DATETIME_KEY, new Date());
    model.put(ReportExcelView.SELLERREPORT_BASAR_NAME_KEY, reportService.getBasarName());
    return new ModelAndView(new ReportExcelView(), model);
  }
  
  @GetMapping("/receipt")
  public ModelAndView receipt() {
    Map<String, Object> model = new HashMap<>();
    model.put(ReceiptPdfView.SELLERREPORT_MODEL_KEY, reportService.report());
    model.put(ReceiptPdfView.SELLERREPORT_BASAR_NAME_KEY, reportService.getBasarName());
    return new ModelAndView(new ReceiptPdfView(), model);
  }
}
