package net.lueckonline.basar.basarserver;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .authorizeRequests()
      .antMatchers(
          "/favicon.ico",
//          "/js/**",
          "/css/**",
          "/*.png",
          "/webjars/**").permitAll()
        .anyRequest().authenticated()
        .and()
      .formLogin()
        .loginPage("/login")
        .permitAll()
//        .and()
//      .logout()
//        .clearAuthentication(true)
//        .invalidateHttpSession(true)
//        .logoutUrl("/logout")
//        .permitAll()
        ;
  }

  
}
