package net.lueckonline.basar.basarserver.db.maintenance;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import net.lueckonline.basar.basarserver.maintenance.MaintenanceService;

@Component
public class DbMaintenanceServiceUsingOSCommand implements MaintenanceService {

  private static final Logger logger = LogManager.getLogger();
  
  @Value("${datasource.username}")
  String mysqlUser;
  
  @Value("${datasource.db}")
  String mysqlDbName;
  
  @Value("${datasource.password}")
  String mysqlPassword;
  
  @Value("${datasource.host}")
  String mysqlHost;
  
  @Value("${upload.temp.directory}")
  String uploadDirectory;
  
  @Autowired
  JdbcTemplate jdbcTemplate;
  
  @Override
  public void importFile(MultipartFile file) throws Exception {
    Path tempPath = Paths.get(uploadDirectory, file.getName());
    file.transferTo(tempPath);

    try {
      String cmd = MessageFormat.format("mysql -u {0} --password={1} --protocol=tcp -h {2} {3} < {4}", mysqlUser, mysqlPassword, mysqlHost, mysqlDbName, tempPath.toAbsolutePath().toString());
      
      Process process = Runtime.getRuntime().exec(new String[]{"/bin/bash","-c",cmd});
      if(process.waitFor() != 0) {
        String errMsg = IOUtils.toString(process.getInputStream(), "UTF-8");
        throw new Exception("Error executing import:"+errMsg);
      }
    } finally {
      Files.deleteIfExists(tempPath);
    }
  }

  @Override
  public void exportFile(OutputStream outStream) throws IOException {
    final String cmd = MessageFormat.format("mysqldump -u {0} --password={1} --protocol=tcp -h {2} {3}", mysqlUser, mysqlPassword, mysqlHost, mysqlDbName);
    
    Process process = Runtime.getRuntime().exec(new String[] {"/bin/bash", "-c", cmd});
    
    try {
      //the process will only exit if there was an error. Otherwise it will stay open until the output is read. But we're waiting 
      //for one second to make sure the process has time to check that an error occured
      boolean exited = process.waitFor(1, TimeUnit.SECONDS);
      
      if(exited && process.exitValue() != 0) 
        throw new IllegalStateException(MessageFormat.format("Error while trying to export db: {0}", IOUtils.toString(process.getErrorStream(), "UTF-8")));
      else 
        process.getInputStream().transferTo(outStream);
      
    } catch (IllegalStateException |InterruptedException e ) {
      logger.atError().withThrowable(e).log("Error during export");
      outStream.write("Error occured while exporting. Please ask the administrator for advice".getBytes("UTF-8"));
    }
  }

  @Override
  public boolean canImport(MultipartFile file) {
    return file != null && StringUtils.endsWith(file.getOriginalFilename(), ".sql");
  }

  @Override
  public boolean canExport(MediaType mediaType) {
    return mediaType != null && MediaType.parseMediaType("application/sql").includes(mediaType);
  }

}
