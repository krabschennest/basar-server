package net.lueckonline.basar.basarserver.maintenance;

import java.io.IOException;
import java.io.OutputStream;

import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public interface MaintenanceService {

  void importFile(MultipartFile file) throws Exception;
  
  void exportFile(OutputStream outStrema) throws IOException;
  
  boolean canImport(MultipartFile file);
  
  boolean canExport(MediaType mediaType);
}
