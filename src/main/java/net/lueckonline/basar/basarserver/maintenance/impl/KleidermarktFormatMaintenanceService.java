package net.lueckonline.basar.basarserver.maintenance.impl;

import static org.apache.commons.lang3.StringUtils.substring;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import net.lueckonline.basar.basarserver.maintenance.MaintenanceService;

@Component
public class KleidermarktFormatMaintenanceService implements MaintenanceService {

  private static final Logger logger = LogManager.getLogger();  
  
  private static final MediaType EXPORTED_MEDIA_TYPE = MediaType.parseMediaType("application/vnd.kleidermarkt+json;charset=UTF-8;encoding=gzip");
  
  @Autowired
  DataSource dataSource;
  
  @Autowired
  ObjectMapper objectMapper;
  
  @Override
  public void importFile(MultipartFile file) throws Exception {

    logger.atInfo().log("importing file {}", () -> file.getOriginalFilename());
    
    try (var con = dataSource.getConnection(); var inputStream = new GZIPInputStream(file.getInputStream())) {
      
      var root = objectMapper.readTree(inputStream);
      var basarId = root.path("id").asLong();
      var basarName = root.path("name").asText();
      
      logger.atDebug().log("Creating database schema");
      ScriptUtils.executeSqlScript(con, new ClassPathResource("/db/migrations/mysql.sql"));

      con.setAutoCommit(false);

      logger.atTrace().log("Starting transaction for import of basar {} with name {} ", basarId, basarName);
      
      try {
        final var basarStmt = con.prepareStatement("INSERT INTO basar (nid,basarId,name,shortname,open,prec,showSize) VALUES (?, ?, ?, ?, 1, 0.1, 1)");
        basarStmt.setLong(   1, basarId);
        basarStmt.setLong(   2, basarId);
        basarStmt.setString( 3, substring(basarName,0,50));
        basarStmt.setString( 4, shortenName(basarName));
        basarStmt.executeUpdate();
  
        final var artikelStmt = con.prepareStatement("INSERT INTO basar_items (basar,vk,article,price,description,size,code,locked) VALUES (?,?,?,?,?,?,?,0)");
        
        root.path("verkäufer").elements().forEachRemaining(verkäufer -> {
          
          final var vkId = verkäufer.get("id").asLong();
          logger.atTrace().log("Starting importing articles of seller {}", vkId);
          int artikelNummer=1;
          for(var it = verkäufer.get("artikel").elements(); it.hasNext(); artikelNummer++) {
            final var artikel = it.next();
            final var artikelId = artikel.get("id").asLong();
            try {
              
              logger.atTrace().log("importing article {}", () -> artikel.toString());
              
              artikelStmt.setLong(1, basarId);
              artikelStmt.setLong(2, vkId);
              // not relevant for kleidermarkt as the artikel as an id (which may be the same as the code but the article MUST always be identifyable by the code
              artikelStmt.setInt(3, artikelNummer);
              artikelStmt.setDouble(4, artikel.get("preis").asDouble());
              artikelStmt.setString(5, artikel.get("beschreibung").asText());
              artikelStmt.setString(6, artikel.path("groesse").asText(null));
              artikelStmt.setString(7, artikel.get("code").asText());
              artikelStmt.addBatch();
            } catch (SQLException e) {
              throw new IllegalStateException(String.format("Error while trying to prepare batch statment for verkäufer %d and artikel %d", vkId, artikelId), e);
            }
          }
        });
        
        artikelStmt.executeBatch();
        
        con.commit();
      
      } catch (Exception e) {
        con.rollback();
        throw e;
      }
    }
  }
  
  private String shortenName(String basarname) {
    return substring(basarname
            .replaceAll("Frühjahrs?", "F")
            .replaceAll("Herbst", "H")
            .replaceAll("(k|K)leidermarkt", "KM")
           , 0, 20);
  }

  @Override
  public void exportFile(OutputStream outStream) throws IOException {
    
    var jdbcTemplate = new JdbcTemplate(dataSource);
    
    final var om   = new ObjectMapper();
    final var root = om.createObjectNode();
    
    jdbcTemplate.query("SELECT basarId,name FROM basar" , rs -> {
      root.put("id", rs.getLong("basarId"));
      root.put("name", rs.getString("name"));
      root.set("verkäufer", om.createArrayNode());
    });
    
    jdbcTemplate.query(
        "SELECT vk,article,price,description,size,bi.code as biCode,bs.code as soldCode "
      + "  from basar_items bi "
      + "  left outer join basar_sold bs on bs.code = bi.code"
      + "  where bi.basar = ? "
      + "  order by vk,article ", (row,i) -> {
        final var artikelNode = om.createObjectNode();
        artikelNode.put("beschreibung", row.getString("description"));
        artikelNode.put("preis", row.getFloat("price"));
        artikelNode.put("code", row.getString("biCode"));
        artikelNode.put("verkauft", row.getString("soldCode") != null);
        var size = row.getString("size");
        if(size != null) artikelNode.put("groesse", size);
        return Map.entry(row.getLong("vk"), artikelNode);
      },new Object[] {root.get("id").asLong()}).stream().collect(Collectors.toMap(e-> e.getKey(), e -> List.of(e.getValue()), (existing,toAdd) -> {
        var list = new ArrayList<ObjectNode>(existing);
        list.addAll(toAdd);
        return list;
      })).forEach((vkId, articles) -> {
        var sellerNode = om.createObjectNode();
        sellerNode.put("id", vkId);
        sellerNode.putArray("artikel").addAll(articles);
        root.withArray("verkäufer").add(sellerNode);
      });

    om.writerWithDefaultPrettyPrinter()
      .writeValue(new GZIPOutputStream(outStream), root);
  }

  @Override
  public boolean canImport(MultipartFile file) {
    return file != null && StringUtils.endsWith(file.getOriginalFilename(), ".json.gz");
  }

  @Override
  public boolean canExport(MediaType mediaType) {
    return mediaType != null && EXPORTED_MEDIA_TYPE.includes(mediaType);
  }

}
