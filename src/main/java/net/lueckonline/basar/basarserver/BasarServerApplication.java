package net.lueckonline.basar.basarserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasarServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasarServerApplication.class, args);
	}
}
