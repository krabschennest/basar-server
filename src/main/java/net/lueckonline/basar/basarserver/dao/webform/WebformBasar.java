package net.lueckonline.basar.basarserver.dao.webform;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


/**
  MariaDB [basar]> describe basar;
  +-----------+----------------+------+-----+---------+----------------+
  | Field     | Type           | Null | Key | Default | Extra          |
  +-----------+----------------+------+-----+---------+----------------+
  | nid       | smallint(6)    | NO   |     | NULL    |                |
  | basarID   | int(11)        | NO   | PRI | NULL    | auto_increment |
  | open      | tinyint(1)     | YES  |     | 0       |                |
  | openTill  | datetime       | YES  |     | NULL    |                |
  | name      | varchar(50)    | YES  |     | NULL    |                |
  | shortname | varchar(20)    | YES  |     | NULL    |                |
  | prec      | float unsigned | NO   |     | NULL    |                |
  | showSize  | int(11)        | YES  |     | NULL    |                |
  | config    | text           | YES  |     | NULL    |                |
  +-----------+----------------+------+-----+---------+----------------+
 * @author thuri
 *
 */

@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name="basar")
@Builder(toBuilder = true)
public class WebformBasar implements Serializable {
  
  private static final long serialVersionUID = 7459522202447143579L;

  @Column(name="basarID") 
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer basarId;
  
  @Column(name="nid") @NonNull @NotNull 
  private Integer nodeId;
  
  private Boolean open;
  
  @Column(name="opentill")
  private Date openTill;
  
  private String name;
  
  private String shortname;
  
  @Column(name="prec") @NonNull @NotNull
  private Float pricePrecision;
  
  @Column(name="showsize")
  private Integer showSize;
  
  private String config;
  
  @OneToMany(mappedBy="id.basar", cascade = CascadeType.ALL)
  private List<WebformSubmittedData> submittedData;
  
  public List<WebformSubmittedData> getSubmittedData() {
    if(this.submittedData == null) this.submittedData = new ArrayList<>();
    
    return this.submittedData;
  }
  
  @OneToMany(mappedBy="basar", cascade = CascadeType.ALL)
  private List<BasarSold> basarSold;
  
  public List<BasarSold> getBasarSold() {
    if(this.basarSold == null) this.basarSold = new ArrayList<>();
    
    return this.basarSold;
  }
  
}
