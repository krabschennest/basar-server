package net.lueckonline.basar.basarserver.dao.webform;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WebformBasarRepository extends PagingAndSortingRepository<WebformBasar, Integer> {

  default WebformBasar findSingleBasar() {
    List<WebformBasar> basars = this.findAll(PageRequest.of(0, 2, Direction.DESC, "nodeId")).get().collect(Collectors.toList());
    if(basars.size() != 1) throw new IllegalStateException("There must be exactly one basar in the database");
    return basars.get(0);
  }

}
