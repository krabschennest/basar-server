package net.lueckonline.basar.basarserver.dao.webform;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/*
 * MariaDB [basar]> describe basar_sold;
 * +-----------+-------------+------+-----+---------------------+-------------------------------+
 * | Field     | Type        | Null | Key | Default             | Extra                         |
 * +-----------+-------------+------+-----+---------------------+-------------------------------+
 * | basar     | int(11)     | NO   | MUL | NULL                |                               |
 * | hwID      | tinyint(4)  | NO   |     | NULL                |                               |
 * | recno     | int(11)     | NO   |     | NULL                |                               |
 * | code      | varchar(25) | NO   | PRI | NULL                |                               |
 * | timestamp | timestamp   | NO   |     | current_timestamp() | on update current_timestamp() |
 * +-----------+-------------+------+-----+---------------------+-------------------------------+
 * 5 rows in set (0.002 sec)
 */

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basar_sold")
@Builder(toBuilder=true)
public class BasarSold {
  
  @ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
  @JoinColumn(name="basar", referencedColumnName="basarID")
  @NonNull
  private WebformBasar basar;
  
  private short hwID;
  
  private int recno;

  @Id
  private String code;
  
  private Date timestamp;

}
