package net.lueckonline.basar.basarserver.dao.webform;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Embeddable
public class WebformSubmittedDataId implements Serializable {
  
  private static final long serialVersionUID = 4631063066340048404L;
  
  @ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
  @JoinColumn(name="nid", referencedColumnName="nid")
  @NonNull
  private WebformBasar basar;
  
  @NonNull @Column(name="sid")
  private Integer sessionId;
  
  @NonNull @Column(name="cid")
  @Enumerated(EnumType.ORDINAL)
  private WebformField field;

}