package net.lueckonline.basar.basarserver.dao.webform;

import java.util.stream.Stream;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface SalesFigureRepository extends Repository<SalesFigure, Integer> {

//  @Query(value="SELECT vk, count(*) as sold_article_count, sum(price) as sold_sum, 0 as article_count FROM basar_items bi join basar_sold bs on bi.code = bs.code GROUP BY VK", nativeQuery=true)
  
//  @Query(value="select vk, "
//                    + "count(*) - count(bs.code) as count_sold_articles, "
//                    + "count(*)                  as count_total_articles, "
//                    + "sum(price)                as sum_total, "
//                    + "sum(case when bs.code is null then price else 0 end) as sum_sold "
//                    + "from basar_items bi left join basar_sold bs on bi.code = bs.code "
//                    + "group by vk", nativeQuery=true)
  
  @Query(value="select * from "
                + "(select vk, "
                        + "count(*) as count_total_articles, "
                        + "ROUND(sum(price),2) as sum_total "
                 + "from basar_items group by vk) total "
               + "join ("
                 + "select vk, "
                        + "count(*) count_sold_articles, "
                        + "ROUND(sum(price),2) sum_sold "
                 + "from basar_items bi join basar_sold bs on bi.code = bs.code group by vk) sum "
               + "on total.vk = sum.vk", nativeQuery = true)
  public Stream<SalesFigure> getSoldFiguresForSellers();
}
