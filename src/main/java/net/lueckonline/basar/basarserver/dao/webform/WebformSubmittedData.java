package net.lueckonline.basar.basarserver.dao.webform;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;


/**
MariaDB [basar]> describe webform_submitted_data;
+-------+----------------------+------+-----+---------+-------+
| Field | Type                 | Null | Key | Default | Extra |
+-------+----------------------+------+-----+---------+-------+
| nid   | int(10) unsigned     | NO   | PRI | 0       |       |
| sid   | int(10) unsigned     | NO   | PRI | 0       |       |
| cid   | smallint(5) unsigned | NO   | PRI | 0       |       |
| no    | varchar(128)         | NO   | PRI | 0       |       |
| data  | mediumtext           | NO   | MUL | NULL    |       |
+-------+----------------------+------+-----+---------+-------+
5 rows in set (0.001 sec)
 * @author thuri
 */

@Data
@Entity
@NoArgsConstructor
@Table(name = "webform_submitted_data")
public class WebformSubmittedData {

  public WebformSubmittedData(WebformBasar basar, int sessionId, WebformField columnId, String data) {
    this.id = new WebformSubmittedDataId(basar, sessionId, columnId);
    this.data = data;
  }
  
  @EmbeddedId
  WebformSubmittedDataId id; 

  @NonNull @NotNull @Column(name="no")
  private String no = "0";
  
  @NonNull @NotNull
  private String data;

}
