package net.lueckonline.basar.basarserver.dao.webform;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder=true)
@Entity
public class SalesFigure {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int vk;
  private double sumSold;
  private double sumTotal;
  private int countSoldArticles;
  private int countTotalArticles;
}
