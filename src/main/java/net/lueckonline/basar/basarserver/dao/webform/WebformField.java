package net.lueckonline.basar.basarserver.dao.webform;

public enum WebformField {
  
  /*
  MariaDB [basar]> select cid,form_key,name from webform_component;
  +-----+----------------------+-----------------------------------------------------------------+
  | cid | form_key             | name                                                            |
  +-----+----------------------+-----------------------------------------------------------------+
  |   1 | anrede               | Anrede                                                          |
  |   2 | givenname            | Vorname                                                         |
  |   3 | surname              | Name                                                            |
  |   4 | geburtsdatum         | Geburtsdatum                                                    |
  |   5 | mail                 | E-Mail-Adresse                                                  |
  |   6 | street               | Stra�e und Hausnummer                                           |
  |   8 | city                 | Ort                                                             |
  |   9 | phone                | Telefon                                                         |
  |  10 | datenschutzerklarung | Einwilligungserkl�rung zur Verarbeitung personenbezogener Daten |
  |  11 | newsletter           | Newsletter                                                      |
  |  13 | password             | password                                                        |
  |  14 | vk                   | VK                                                              |
  |  15 | plz                  | PLZ                                                             |
  +-----+----------------------+-----------------------------------------------------------------+
  */
  UNUSED0, // 0 is not used but we need the entry in order to use ORDINAL EnumType on the Entity
  SALUTATION, //1
  GIVENNAME , //2
  SURNAME, //3
  DATEOFBIRTH, //4
  EMAIL, //5
  STREET, //6 
  UNUSED7, //There is no 7 but we need the entry in order to use ORDINAL EnumType on the Entity
  CITY, //8 
  PHONE, //9
  PRIVACYNOTICEAGGREEMENT, //10
  NEWSLETTERSUBSCRIPTION, //11
  UNUSED12, //12
  PASSWORD, //13
  SELLERID, //14
  UNUSED15, //15
  POSTCODE; //16
}
