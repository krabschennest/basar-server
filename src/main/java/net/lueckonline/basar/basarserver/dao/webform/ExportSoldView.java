package net.lueckonline.basar.basarserver.dao.webform;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.DeflaterOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64OutputStream;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.view.AbstractView;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

public class ExportSoldView extends AbstractView {

  public static final String EXPORTSOLD_MODEL_KEY = "sold";

  @Override
  protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    
    @SuppressWarnings("unchecked")
    List<BasarSold> soldItems = (List<BasarSold>) model.get(EXPORTSOLD_MODEL_KEY);
    if(soldItems == null || soldItems.isEmpty()) throw new IllegalArgumentException("No export available");
    
    SoldDTO dto = new SoldDTO();
    dto.basar = String.valueOf(soldItems.get(0).getBasar().getBasarId());
    dto.sold = soldItems.stream()
                        .map(s -> SoldItemDTO.builder()
                           .code(s.getCode())
                           .recno(String.valueOf(s.getRecno()))
                           .hwID(String.valueOf(s.getHwID()))
                           .build())
                        .collect(Collectors.toList());
    
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=export_sold.json.gz");
    
    DeflaterOutputStream gzo = new DeflaterOutputStream(new Base64OutputStream(response.getOutputStream()));
    
    ObjectMapper om = new ObjectMapper();
    om.writeValue(gzo, dto);
    
    response.flushBuffer();
  }

  @Data
  static class SoldDTO {
    String basar;
    List<SoldItemDTO> sold;
  }

  @Data
  @Builder
  @NoArgsConstructor
  @AllArgsConstructor
  static class SoldItemDTO {
    String hwID;
    String recno;
    String code;
  }
}
