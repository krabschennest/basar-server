package net.lueckonline.basar.basarserver.dao.webform;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import net.lueckonline.basar.basarserver.report.ReportService;
import net.lueckonline.basar.basarserver.report.SellerReport;

@Component
@Transactional
public class WebformBasedReportService implements ReportService {

  @Value("${basar.foundationShareFraction:0.15}")
  private float foundationShareFraction;
  
  private final WebformBasarRepository basarRepository;
  
  private final SalesFigureRepository salesFiguresRepository;
  
  private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

  public WebformBasedReportService(WebformBasarRepository basarRepository, SalesFigureRepository salesFiguresRepository) {
    super();
    this.basarRepository = basarRepository;
    this.salesFiguresRepository = salesFiguresRepository;
  }

  @Override
  public List<SellerReport> report() {
    
    final WebformBasar basar = basarRepository.findSingleBasar();
    
    Map<Integer, SalesFigure> salesFigureMappedByVk = salesFiguresRepository.getSoldFiguresForSellers()
                                                     .collect(Collectors.toMap(SalesFigure::getVk, Function.identity()));
    
    //we need to flatten the results because the value of each field of each submission has a separate row in the db
    final Map<Integer, SellerReport> sellersBySid = new HashMap<Integer, SellerReport>();
    
    basar.getSubmittedData().stream().forEach(wsd -> {
    
      final SellerReport sellerReport = sellersBySid.computeIfAbsent(wsd.getId().getSessionId(), k -> SellerReport.builder().foundationShareFraction(this.foundationShareFraction).build());
      
      switch(wsd.getId().getField()) {
        case SALUTATION : sellerReport.setSalutation(wsd.getData());                break;
        case GIVENNAME  : sellerReport.setGivenname(wsd.getData());                 break;
        case SURNAME    : sellerReport.setSurname(wsd.getData());                   break;
        case DATEOFBIRTH: sellerReport.setBirthday(parseBirthDay(wsd.getData()));   break;
        case EMAIL      : sellerReport.setMail(wsd.getData());                      break;
        case STREET     : sellerReport.setStreet(wsd.getData());                    break;
        case CITY       : sellerReport.setCity(wsd.getData());                      break;
        case POSTCODE   : sellerReport.setPostCode(wsd.getData());                  break;
        case PHONE      : sellerReport.setPhone(wsd.getData());                     break;
        case SELLERID   : {
          sellerReport.setVk(Integer.parseInt(wsd.getData()));
          SalesFigure sf = salesFigureMappedByVk.get(sellerReport.getVk());
          if(sf != null) {
            sellerReport.setSoldSum(Money.of(sf.getSumSold(), "EUR"));
            sellerReport.setArticleCount(sf.getCountTotalArticles());
            sellerReport.setSoldArticleCount(sf.getCountSoldArticles());
          }
          break;
        }
        default: break;
      }
    });
    
    return sellersBySid.values().stream()
    .sorted(Comparator.comparing(SellerReport::getVk)).collect(Collectors.toList());
  }
  
  public List<BasarSold> exportSold() {

    WebformBasar findSingleBasar = this.basarRepository.findSingleBasar();
    return findSingleBasar.getBasarSold();
  }

  private Date parseBirthDay(String birthdayStr) {
    try {
      return dateFormat.parse(birthdayStr);
    } catch (ParseException e) {
      return null;
    }
  }

  @Override
  public String getBasarName() {
    return basarRepository.findSingleBasar().getShortname();
  }

  public float getFoundationShareFraction() {
    return foundationShareFraction;
  }

  public void setFoundationShareFraction(float foundationShareFraction) {
    this.foundationShareFraction = foundationShareFraction;
  }
}
