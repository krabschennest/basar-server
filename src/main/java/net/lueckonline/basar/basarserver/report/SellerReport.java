package net.lueckonline.basar.basarserver.report;


import java.util.Date;

import javax.money.Monetary;

import org.javamoney.moneta.Money;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(toBuilder=true)
public class SellerReport {
  
  @Builder.Default
  // don't use a NoArgsConstructor because the following initialization doesn't work there together with the builder pattern
  private Money soldSum = Money.of(0.0, "EUR");
  @Builder.Default
  private int articleCount = 0;
  @Builder.Default
  private int soldArticleCount = 0;
  @Builder.Default
  private float foundationShareFraction = 0.0f;
  
  public Money getSoldSum() {
    return soldSum.with(Monetary.getDefaultRounding());
  }
  
  public Money getFoundationShare() {
    return soldSum.multiply(foundationShareFraction).with(Monetary.getDefaultRounding());
  }
  
  public Money getPayOut() {
    return soldSum.subtract(getFoundationShare()).with(Monetary.getDefaultRounding());
  }
  
  private int vk;
  
  private String salutation;
  private String surname;
  private String givenname;
  private String phone;
  private String street;
  private String postCode;
  private String city;
  private String mail;
  private Date   birthday;
  
}
