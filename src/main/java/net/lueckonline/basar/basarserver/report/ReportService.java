package net.lueckonline.basar.basarserver.report;

import java.util.List;

import net.lueckonline.basar.basarserver.dao.webform.BasarSold;

public interface ReportService {
  
  public List<SellerReport> report();
  
  public List<BasarSold> exportSold();
  
  public String getBasarName();
}
