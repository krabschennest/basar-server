package net.lueckonline.basar.basarserver.report;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.DateFormatConverter;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

public class ReportExcelView extends AbstractXlsxView {

  public static final String SELLERREPORT_MODEL_KEY = "sellerReport";
  public static final String SELLERREPORT_BASAR_NAME_KEY = "basarName";
  public static final String SELLERREPORT_EXPORT_DATETIME_KEY = "exportDateTime";
  
  @Override
  protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
    
    int rowIdx = 0;
    
    @SuppressWarnings("unchecked") //TODO: make the model an actual calls itself
    List<SellerReport> sellerList = (List<SellerReport>) model.get(SELLERREPORT_MODEL_KEY);

    Sheet basarSheet = workbook.createSheet("Basarabrechnung");
    
    CellStyle currencyCellStyle = workbook.createCellStyle();
    DataFormat currencyDateFormat = workbook.createDataFormat();
    currencyCellStyle.setDataFormat(currencyDateFormat.getFormat("0.#0 [$€-407]"));
    
    CellStyle dateCellStyle = workbook.createCellStyle();
    DataFormat dateDataFormat = workbook.createDataFormat();
    dateCellStyle.setDataFormat(dateDataFormat.getFormat(DateFormatConverter.convert(Locale.GERMANY, "dd.MM.yyyy")));
    
    basarSheet.setDefaultColumnStyle(0, currencyCellStyle);
    basarSheet.setDefaultColumnStyle(1, currencyCellStyle);
    basarSheet.setDefaultColumnStyle(2, currencyCellStyle);
    
    basarSheet.createRow(rowIdx++).createCell(0).setCellValue((String) model.get(SELLERREPORT_BASAR_NAME_KEY));
    Cell exportDateCell = basarSheet.createRow(rowIdx++).createCell(0);
    exportDateCell.setCellValue((Date) model.get(SELLERREPORT_EXPORT_DATETIME_KEY));
    exportDateCell.setCellStyle(dateCellStyle);
    
    basarSheet.setDefaultColumnStyle(10, dateCellStyle);
    
    Row headerRow = basarSheet.createRow(rowIdx++);
    int headerColumnIdx=0;
    //the expression headerColumnIdx++ is used instead of literal indices because we don't need to change
    //the literal expressions when the column order is changed or we need to add columns in between
    headerRow.createCell(headerColumnIdx++).setCellValue("verkaufsSumme");
    headerRow.createCell(headerColumnIdx++).setCellValue("anteilVerein");
    headerRow.createCell(headerColumnIdx++).setCellValue("auszahlungsBetrag");
    headerRow.createCell(headerColumnIdx++).setCellValue("artikelZahl"); 
    headerRow.createCell(headerColumnIdx++).setCellValue("plz");
    headerRow.createCell(headerColumnIdx++).setCellValue("vk");
    headerRow.createCell(headerColumnIdx++).setCellValue("phone");
    headerRow.createCell(headerColumnIdx++).setCellValue("city");
    headerRow.createCell(headerColumnIdx++).setCellValue("street");
    headerRow.createCell(headerColumnIdx++).setCellValue("mail");
    headerRow.createCell(headerColumnIdx++).setCellValue("geburtsdatum");
    headerRow.createCell(headerColumnIdx++).setCellValue("surname");
    headerRow.createCell(headerColumnIdx++).setCellValue("givenname");
    headerRow.createCell(headerColumnIdx++).setCellValue("anrede");

    for(SellerReport sellerReport : sellerList) {
      Row sellerRow = basarSheet.createRow(rowIdx++);
      int sellerColumnIdx=0;
      
      //the expression sellerColumnIdx++ is used instead of literal indices because we don't need to change
      //the literal expressions when the column order is changed or we need to add columns in between
      
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getSoldSum().getNumber().doubleValue());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getFoundationShare().getNumber().doubleValue());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getPayOut().getNumber().doubleValue());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getArticleCount());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getPostCode());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getVk());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getPhone());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getCity());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getStreet());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getMail());
      //TODO: Make sure to handle dates before 1900 so that excel can handle it.
      //otherwise birthays before 01-01-1900 will be set as null (doesn't throw an exception 
      //but getting the date value just returns null
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getBirthday());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getSurname());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getGivenname());
      sellerRow.createCell(sellerColumnIdx++).setCellValue(sellerReport.getSalutation());
    }
    
    for(int i=3; i< headerColumnIdx;i++) {
      basarSheet.autoSizeColumn(i);
    }
    
    response.setHeader("Content-Disposition", "attachment; filename=abrechnung.xlsx");
  }
}
